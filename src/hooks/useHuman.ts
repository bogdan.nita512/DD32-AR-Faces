import createRAF from "@solid-primitives/raf";
import { createSignaledWorker } from "@solid-primitives/workers";
import { Config, Human, Result, defaults } from "@vladmandic/human"
import { createEffect, createMemo, createSignal, on } from "solid-js"

export const saneDefaults: Partial<Config> =
{
	filter: { 
        enabled: false, 
        equalization: false,
        flip: false, 
    },
	face: { 
        enabled: false, 		
        iris: { enabled: false },
        emotion: { enabled: false },
        liveness: { enabled: false },
        antispoof: { enabled: false },
        description: { enabled: false },
        mesh: { enabled: false },
        detector: { 
            enabled: false, 
            rotation: false, 
            return: false 
        },
    },
	body: { enabled: false },
	hand: { enabled: false },
	object: { enabled: false },
	gesture: { enabled: false },
	segmentation: { enabled: false },
}

export const useHuman = 
    (props: { config? : Partial<Config>}) => 
    {
        const [human, setHuman] = createSignal(new Human({ ...saneDefaults, ...props.config}))
        const [video, setVideo] = createSignal<HTMLVideoElement>(null);

        const [result, setResult] = createSignal<Result>(null, { equals: false })

        const [running, start, stop] = createRAF(() => { setResult(human().next(human().result)); })

        createEffect(on(video,() => { if(video()) { human().video(video()); }}))

        return { human, start, running, stop, video, setVideo, result }
    }