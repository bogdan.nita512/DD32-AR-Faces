import { Component, createSignal, onCleanup, Accessor, createMemo, onMount, Setter, createEffect, JSX, lazy } from "solid-js";
import Human, { Box, FaceResult } from "@vladmandic/human";
import usePopper from "solid-popper";
import { Instance, Options, ModifierPhases } from '@popperjs/core';
// import { singleQuotes, doubleQuotes } from "../assets/content.json";
import { sample } from "lodash";
import { ReactiveMap } from "@solid-primitives/map";
import { createShortcut } from "@solid-primitives/keyboard";
import { Dynamic } from "solid-js/web";

// import Something from "../assets/SingleQuotes/SingleQuote_01.svg?component-solid";
// const singleQuotes = import.meta.glob('../src/assets/SingleQuotes/*.svg', { as: 'component-solid' })
// singleQuotes.SingleQuote_01()
// const quotes = [
//     () => import("../assets/SingleQuotes/SingleQuote_01.svg?component-solid"),

// ]

type SVGQuote = { default: Component<JSX.SvgSVGAttributes<SVGSVGElement>> }
const modules = import.meta.glob<SVGQuote>('../assets/SingleQuotes/*.svg', { as: 'component-solid' })

const quotes = Object.entries(modules).map(([key, value]) => {
    return { name: key, SvgComponent: lazy(value) }
})

export const applyArrowHide = {
    name: 'applyArrowHide',
    enabled: false,
    phase: 'write' as ModifierPhases,
    fn({ state }) {
        const { arrow } = state.elements;

        if (arrow) {
            if (state.modifiersData.arrow.centerOffset !== 0) {
                arrow.setAttribute('data-hide', '');
            } else {
                arrow.removeAttribute('data-hide');
            }
        }
    },
}

export const config: Partial<Options> =
{
    placement: "auto-start",
    modifiers: [
        { name: 'offset', options: { offset: [0, 25], }, },
        { name: 'arrow', options: { element: ".arrow", padding: 5 }, },
        { name: 'flip', options: { allowedPlacements: ["left", "right"], }, },
        { name: 'computeStyles', options: { gpuAcceleration: true, adaptive: true }, },
        { ...applyArrowHide }
    ]
}

const colors = ['#008c95', '#a9c23f', '#f0b323', '#ee5340']


export const Bubble: Component<{ face: Accessor<FaceResult>, context: ReactiveMap<number, Setter<String>>, debug: Accessor<boolean> }> =
    (props) => {

        const [content, setContent] = createSignal("");
        const id = createMemo(() => props.face().id);

        onMount(() => {
            props.context.set(id(), setContent)
            if (id() % 2 == 1 && props.context.has(id() - 1)) {
                const setSiblingContent = props.context.get(id() - 1);

                // const quote = sample(doubleQuotes);
                // setSiblingContent(quote[0])
                // setContent(quote[1]);
            }
            else {
                // setContent(sample(singleQuotes))
            }
        })
        onCleanup(() => {
            props.context.delete(id())
            if (props.context.has(id() - 1)) {
                const setSiblingContent = props.context.get(id() - 1);
                // setSiblingContent(sample(singleQuotes))
            }
        })

        const [anchor, setAnchor] = createSignal<HTMLDivElement>();
        const [popper, setPopper] = createSignal<HTMLDivElement>();
        const [arrow, setArrow] = createSignal<HTMLDivElement>();

        const instance = usePopper(anchor, popper, config)
        const toPercentage = (value: number) => `${value * 100}%`
        const box = createMemo(
            () => {
                const [left, top, width, height] =
                    props.face().boxRaw.map(toPercentage)
                instance()?.update();

                return { left, top, width, height }
            }
        )

        const details = createMemo(
            () => {
                const [left, top, width, height] = props.face().boxRaw;
                const center = {
                    x: left + width / 2,
                    y: top + height / 2
                }
                return { left, top, width, height, center }
            }
        )

        const { name, SvgComponent } = sample(quotes);

        const [svg, setSVG] = createSignal<SVGAElement>();
        const [text, setText] = createSignal<HTMLDivElement>();
        onMount(() => {
            if (svg() && text()) {
                const base = svg().getElementsByClassName("cls-1")[0];
                const box = base.getBoundingClientRect();
                console.log(box)
                text().style.top = toPercentage(box.top);
                text().style.left = toPercentage(box.left);
                text().style.width = toPercentage(box.width);
                text().style.height = toPercentage(box.height);
            }
        })

        return (
            <>
                <div
                    ref={setAnchor}
                    class="absolute"
                    classList={{
                        "border-8 border-dashed rounded-xl z-20": props.debug()
                    }}
                    // class="absolute"
                    style={box()}
                />
                {/* <div
                    // ref={setPopper}
                    class="tooltip w-1/5 z-20"
                    style={{
                        background: sample(colors),
                    }}
                >
                    {content()}
                    <div ref={setArrow} class="text-2xl arrow" data-popper-arrow></div>
                </div> */}
                <div ref={setPopper} class="w-1/5 object-fill">
                    <div ref={setText} class=" z-20 text-white" classList={{ "p2": true }}>
                        {/* {content()} */}
                    </div>
                    {/* <Dynamic component={sample(quotes).SvgComponent} /> */}
                    <SvgComponent
                        ref={setSVG}
                        class="absolute top-0 z-10"
                        style={{ transform: `scale(${[popper().hasAttributes()] ? -1 : 1},1)` }}>

                        {/* <text>{content()}</text> */}
                    </SvgComponent>
                </div>
                {sample(quotes).SvgComponent}

                {/* <div
                    class="speech half"
                    style={{
                        left: box().left,
                        top: box().top,
                        scale: mapRange([0, 0.75], [0.25, 1.25], props.face().boxRaw[2]),

                        // "max-width": toPercentage(props.face().boxRaw[2] * 2)
                    }}
                // ref={(ref) => { ref.style.background = sample(colors); }}
                >
                    {content()}
                </div> */}
            </>
        )
    }