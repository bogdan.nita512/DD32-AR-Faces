import { createContextProvider } from "@solid-primitives/context";
import { ReactiveMap } from "@solid-primitives/map";
import { debounce, ScheduleCallback, Scheduled } from "@solid-primitives/scheduled";
import { FaceResult } from "@vladmandic/human";
import { chunk, clamp, head, mapValues, sample, sortBy } from "lodash";
import { Accessor, Component, createEffect, createMemo, createSignal, JSX, lazy, on, onCleanup, onMount, Setter, Show } from "solid-js";
import { Dynamic } from "solid-js/web";
import usePopper from "solid-popper";
import { Instance, Options, ModifierPhases } from '@popperjs/core';

export type SVGComponent = Component<JSX.SvgSVGAttributes<SVGSVGElement>>;
export type SVGModule = { default: SVGComponent }
export const makeLazy = (component: () => Promise<SVGModule>) => lazy(component);
export type LazySVG = ReturnType<typeof makeLazy>;

const singleModules = import.meta.glob<SVGModule>('../assets/SingleQuotes/*.svg', { as: 'component-solid' })
const doubleModules = import.meta.glob<SVGModule>('../assets/DoubleQuotes/*.svg', { as: 'component-solid' })

const mapToComponents = (modules: typeof singleModules | typeof doubleModules) =>
    Object.entries(modules).map(([index, value]) => ({ name: index, svg: lazy(value) }))

const singleQuotes = mapToComponents(singleModules);
const doubleQuotes = chunk(sortBy(mapToComponents(doubleModules), (e) => e.name), 2);

const toPixel = (value: number) => `${value}px`
const toPercentage = (value: number) => `${value * 100}%`

export const [QuoteProvider, useQuoteContext] = createContextProvider((props: {}) => {
    const context = new ReactiveMap<number, { svg: SVGComponent, dispose: Scheduled<[]> }>;
    return { context };
});

export const Quote: Component<{
    debug?: Accessor<boolean>
    face: Accessor<FaceResult>
    video: Accessor<HTMLVideoElement>
}> =
    (props) => {
        const debug = createMemo(() => props.debug() ?? false)

        const { context } = useQuoteContext();
        const instanceId = createMemo(() => props.face().id);
        const siblingId = createMemo(() => props.face().id - 1);

        const hasInstance = createMemo(() => context?.has(instanceId()) ?? false)
        const hasSibling = createMemo(() => context?.has(siblingId()) ?? false)

        const instance = createMemo(() => context?.get(instanceId()));
        const sibling = createMemo(() => context?.get(siblingId()))

        const dispose = debounce(() => context.delete(instanceId()), 1000)

        onMount(() => {
            if (hasInstance()) {
                instance().dispose.clear();
            }
            else if (instanceId() % 2 == 1 && hasSibling()) {
                const quote = sample(doubleQuotes);
                context.set(siblingId(), { ...sibling(), svg: quote[0].svg })
                context.set(instanceId(), { dispose, svg: quote[1].svg })
            }
            else {
                const quote = sample(singleQuotes);
                context.set(instanceId(), { dispose, svg: quote.svg })
            }
        })

        onCleanup(() => {
            if (hasInstance()) { instance().dispose() }

            if (instanceId() % 2 == 1 && hasSibling()) {
                const quote = sample(singleQuotes);
                context.set(siblingId(), { ...sibling(), svg: quote.svg })
            }
        })

        const [anchor, setAnchor] = createSignal<HTMLDivElement>();
        const [content, setContent] = createSignal<SVGSVGElement>();
        createEffect(() => {
            if (props.video() && anchor() && content()) {
                const videoRect = props.video().getBoundingClientRect();
                const detection = new DOMRect(...props.face().boxRaw);

                detection.x = videoRect.x + detection.x * videoRect.width;
                detection.y = videoRect.y + detection.y * videoRect.height;
                detection.width *= videoRect.width;
                detection.height *= videoRect.height;

                const contentRect = new DOMRect(
                    detection.left + detection.width,
                    detection.top - detection.height / 2,
                    detection.width,
                    detection.height,
                )

                if (contentRect.top < 0) {
                    contentRect.y += 0 - contentRect.top;
                }
                if (contentRect.bottom > window.innerHeight) {
                    contentRect.y -= contentRect.bottom - window.innerHeight;
                }
                if (contentRect.left < 0) {
                    contentRect.x += 0 - contentRect.left;
                }
                if (contentRect.right > window.innerWidth) {
                    contentRect.x -= contentRect.right - window.innerWidth;
                }

                anchor().style.top = toPixel(detection.top);
                anchor().style.left = toPixel(detection.left);
                anchor().style.width = toPixel(detection.width);
                anchor().style.height = toPixel(detection.height);

                content().style.top = toPixel(contentRect.top);
                content().style.left = toPixel(contentRect.left);
                content().style.width = toPixel(contentRect.width);
                content().style.height = toPixel(contentRect.height);
            }
        })


        return (
            <>
                <div
                    ref={setAnchor}
                    class="fixed border-8 border-dashed rounded-xl z-20"
                    classList={{ "invisible": !debug() }}
                />
                <Show when={hasInstance()}>
                    <Dynamic
                        class="fixed"
                        ref={setContent}
                        component={instance().svg}
                        style={{ "max-width": "25vw", "min-width": "5vw", }} />
                </Show>

            </>
        )
    }