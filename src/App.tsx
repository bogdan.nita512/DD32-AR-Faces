import { Component, createMemo, createSignal, onMount, Setter } from 'solid-js';
import { saneDefaults, useHuman } from './hooks/useHuman';
import { Webcam } from "solid-webcam";
import { Config } from '@vladmandic/human';
import { Bubble } from './components/Bubble';
import Background from "./assets/Background.svg?component-solid"
import { Key } from '@solid-primitives/keyed';
import { sample } from 'lodash';
import { singleQuotes } from './assets/content.json'
import { ReactiveMap } from '@solid-primitives/map';
import { TransitionGroup } from 'solid-transition-group';
import { createShortcut } from '@solid-primitives/keyboard';
import { LazySVG, Quote, QuoteProvider, SVGComponent } from './components/Quote';
import { Scheduled } from '@solid-primitives/scheduled';

const constraints = {
	aspectRatio: { ideal: 16 / 9 },
	width: { ideal: 1920 },
	height: { ideal: 1080 },
	facingMode: "user"
} as MediaTrackConstraints;

export const config: Partial<Config> =
{
	debug: true,
	backend: "webgl",
	modelBasePath: 'https://cdn.jsdelivr.net/gh/vladmandic/human-models/models/',

	filter: { ...saneDefaults.filter, enabled: true },
	face: {
		...saneDefaults.face,
		enabled: true,
		iris: { enabled: true },
		mesh: { enabled: true },
		detector: {
			...saneDefaults.face.detector,
			enabled: true,
			maxDetected: 5,
			minConfidence: 0.5,
		},
	},
}


export const App: Component = () => {
	const api = useHuman({ config })
	onMount(() => api.start())
	const toggleHuman = createMemo(() => api.running() ? api.stop : api.start)

	const context = new ReactiveMap<number, { svg: SVGComponent, cleanUp: Scheduled<[]> }>();

	const [debug, setDebug] = createSignal(false);
	createShortcut(
		["Shift", "~"],
		() => {

			setDebug(value => !value)
			console.log("Shif + ~ pressed: " + debug())
		},
		{ preventDefault: false },
	);


	return (
		<div class=" bg-slate-100 h-screen w-full flex justify-center items-center overflow-hidden">
			<Background class='absolute bottom-0 z-20 opacity-90 overflow-hidden pointer-events-none' />
			<div class='w-5/6 aspect-video relative'>
				<Webcam
					ref={api.setVideo}
					audio={false}
					class="container rounded-lg shadow-lg z-10"
					preload="auto"
					videoConstraints={constraints}
					onclick={toggleHuman()} />
				<QuoteProvider>
					<Key each={api.result()?.face} by={(face) => face.id}>
						{(face, _) => <Quote face={face} debug={debug} video={api.video} />}
					</Key>
				</QuoteProvider>
			</div>
			<div class='fixed inset-x-0 bottom-0 p-4 z-20 text-black text-sm text-center font-normal antialiased'>
				<div>PP-GZ-RO-0113</div>
				<div>Acest material este destinat profesioniștilor în domeniul sănătății.</div>
			</div>
		</div >
	);
};

