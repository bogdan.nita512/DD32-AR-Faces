import { defineConfig } from 'vite';
import { fileURLToPath, URL } from "url";
import solidPlugin from 'vite-plugin-solid';
import solidSvg from 'vite-plugin-solid-svg'
import path from 'path';


export default defineConfig({
  base: "/DD32-AR-Faces/",
  plugins: [
    solidPlugin(),
    solidSvg({defaultAsComponent: true}),
  ],
  server: {
    port: 3000,
  },
  build: {
    target: 'esnext',
  },
});
